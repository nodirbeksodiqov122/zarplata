import {Button, Menu} from 'antd'
import './menu.less'
import React, {useState} from "react";
import {MailOutlined, MenuFoldOutlined, MenuUnfoldOutlined, DashboardOutlined} from '@ant-design/icons';

const {SubMenu} = Menu

function MainMenu() {

    const [collapsed, setCollapsed] = useState(false)

    const toggleCollapsed = () => {
        setCollapsed(!collapsed)
    }

    return (
        <>

            <Menu
                mode="inline"
            >
                <SubMenu  key="sub1" icon={<DashboardOutlined/>} title="Navigation One">
                    <Menu.Item key="1">Option 5</Menu.Item>
                    <Menu.Item key="2">Option 6</Menu.Item>
                    <Menu.Item key="3">Option 7</Menu.Item>
                    <Menu.Item key="4">Option 8</Menu.Item>
                </SubMenu>
                <SubMenu key="sub2" icon={<MailOutlined/>} title="Navigation One">
                    <Menu.Item key="5">Option 5</Menu.Item>
                    <Menu.Item key="6">Option 6</Menu.Item>
                    <Menu.Item key="7">Option 7</Menu.Item>
                    <Menu.Item key="8">Option 8</Menu.Item>
                </SubMenu>
            </Menu>
        </>
    )
}

export default MainMenu
