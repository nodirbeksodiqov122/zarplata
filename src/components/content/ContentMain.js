import React from "react";
import './content.css'
import {Button, Col, Dropdown, Row, Menu} from "antd";
import {DownOutlined} from '@ant-design/icons'

const ContentMain = function () {
    const menu = (
        <Menu>
            <Menu.Item key="1">
                Маркетинг директор
            </Menu.Item>
            <Menu.Item key="2">
                НДР
            </Menu.Item>
            <Menu.Item key="3">
                Начальник отдела продаж
            </Menu.Item>
            <Menu.Item key="4">
                Начальник отдела логистики
            </Menu.Item>
            <Menu.Item key="4">
                Финансовый директор
            </Menu.Item>
            <Menu.Item key="5">
                Начальник категорийного менеджмента
            </Menu.Item>
            <Menu.Item key="6">
                Коммерческий директор
            </Menu.Item>
            <Menu.Item key="7">
                Заместитель генерального директора
            </Menu.Item>
        </Menu>
    );
    return (
        <>
            <Row>
                <Col span={12}>
                    <Dropdown overlay={menu}>
                        <Button
                            block
                        >
                            Маркетинг директор
                            <DownOutlined/>
                        </Button>
                    </Dropdown>
                </Col>
                <Col span={7}>
                    <Dropdown overlay={menu}>
                        <Button
                            block
                        >
                            Выберите дата
                            <DownOutlined/>
                        </Button>
                    </Dropdown>
                </Col>
                <Col span={5}>
                    <Button block type={"primary"}>
                        Експорт в PDF
                    </Button>
                </Col>
                <Col span={19}>
                    chart
                </Col>
                <Col span={5}>
                    description
                </Col>


            </Row>
        </>
    )
}

export default ContentMain
