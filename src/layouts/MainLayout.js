import React, {useState} from 'react'
import {Button, Layout, notification} from 'antd'
import RightContent from '@/components/RightContent'
import MenuHeader from '@/components/MenuHeader'
import MainMenu from '@/components/menu/Menu'
import basic from '@/constants/basic'
import './MainLayout.css'
import '../components/menu/Menu'
import {MenuUnfoldOutlined, MenuFoldOutlined} from '@ant-design/icons'
import {useSelector} from 'react-redux'
import ContentMain from "@/components/content/ContentMain";
import VerticalBar from "@/components/chart/Chart";

const {Header, Content, Sider} = Layout

// ***********************WEBSOCKET****************************************
const token = localStorage.getItem('token')
// if (token) {
//   const websocket = new WebSocket(`wss://websocket.muno.uz/ws?Authorization=${token}`)
//   websocket.onopen = () => {
//     console.log('Muno socket connecting.....')
//   }
//   websocket.onmessage = (e) => {
//     console.log('WebSocket message received: ', e)
//     notification.warning({
//       message: 'WebSocket Received'
//     })
//   }
// }
// ***********************WEBSOCKET****************************************


export default function MainLayout({children}) {
    const [collapsed, setCollapsed] = useState(false)

    const toggleCollapsed = () => {
        setCollapsed(!collapsed)
    }
    return (
        <div className={`App `}>
            <Layout>
                <Header>
                    <img src={basic.LOGO}/>
                </Header>
                <Layout>
                    <Sider
                        collapsed={collapsed}
                        onCollapse={() => setCollapsed(!collapsed)}
                    >
                        <MainMenu/>
                        <Button type="primary" onClick={toggleCollapsed} style={{marginBottom: 16}}>
                            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                        </Button>
                    </Sider>
                    <Content>
                      <VerticalBar/>
                    </Content>
                </Layout>
            </Layout>
        </div>
    )
}
